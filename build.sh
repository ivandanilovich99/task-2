cd ./api
docker build --build-arg MYSQL_HOST=${MYSQL_HOST} \
             --build-arg MYSQL_PORT=${MYSQL_PORT} \
             --build-arg MYSQL_USER=${MYSQL_USER} \
             --build-arg MYSQL_PASSWORD=${MYSQL_PASSWORD} \
             --build-arg MYSQL_DB=${MYSQL_DB} \
             . \
             -t task-1_api

cd ../frontend
docker build . -t task-1_frontend